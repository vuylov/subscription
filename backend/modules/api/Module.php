<?php

namespace backend\modules\api;
use Yii;

/**
 * api module definition class
 */
class Module extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'backend\modules\api\controllers';
    public $defaultRoute = 'user/index';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        //configure Yii object
        //\Yii::configure($this, require __DIR__.'/config/main.php');
        //\Yii::$app->setComponents(require __DIR__.'/config/main.php');
        \Yii::$app->getRequest()->parsers = ['application/json' => 'yii\web\JsonParser'];
        \Yii::$app->getResponse()->format = \yii\web\Response::FORMAT_JSON;
    }
}
