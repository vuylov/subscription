Тестовое задание Subscription
===============================
###Миграции
В миграции добавлены dummy-пользователи:
* admin:admin
* user:user

## REST API

REST API организовал через модуль.

### Авторизация
Реализована через Basic Auth. Логин - `admin`, пароль - `admmin`.

### Endpoints:
* `GET \api\users` - список всех пользователей
* `GET \api\users?expand=subscription` - список пользователй с информацией о подписке
* `PUT,PATCH \api\users\{id}` - редактирование пользователя (по id).

###Пример JSON-тела запроса на редактирование:
```
{"username":"admin2", "last_name":"Петров", "subscription":{"expired":"31-12-2017 23:59:59"}}
```

## Консольные команды
`php yii subscription/delete-expired` - удаление просроченных на данный момент подписок