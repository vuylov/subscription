<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\datetime\DateTimePicker;

/* @var $this yii\web\View */
/* @var $model common\models\User */
/* @var $subscription common\models\Subscription */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'username')->textInput();?>

    <?= $form->field($model, 'last_name')->textInput();?>

    <?= $form->field($model, 'first_name')->textInput();?>

    <?= $form->field($model, 'middle_name')->textInput();?>

    <?= $form->field($model, 'password_hash')->textInput(['value' => '']);?>

    <?= $form->field($model, 'email')->textInput();?>

    <?= $form->field($subscription, 'expired')->widget(DateTimePicker::className(), [
        'pluginOptions' => [
            'autoclose'=>true,
            'format' => 'dd-mm-yyyy 23:59:59',
            'pickTime' => false,
            'minView' => 2,
            'startDate' => new \yii\web\JsExpression('new Date()')
        ]
    ])?>
    
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

