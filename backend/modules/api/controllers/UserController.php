<?php
/**
 * Created by PhpStorm.
 * User: 6yt9IBka
 * Date: 24.02.2017
 * Time: 19:56
 */

namespace backend\modules\api\controllers;
use common\models\Subscription;
use yii\data\ActiveDataProvider;
use yii\helpers\VarDumper;
use yii\rest\ActiveController;
use yii\filters\auth\HttpBasicAuth;
use common\models\User;
use yii\web\HttpException;

class UserController extends ActiveController
{
    public $modelClass = 'common\models\User';

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => HttpBasicAuth::className(),
            'auth' => function($username, $password){
                if($username === User::USERNAME && $password === User::PASSWORD){
                    return User::findOne(1);
                }
                return null;
            }
        ];
        $behaviors['contentNegotiator'] = [
            'class' => \yii\filters\ContentNegotiator::className(),
            'formats' => [
                'application/json' => \yii\web\Response::FORMAT_JSON,
            ],
        ];
        return $behaviors;
    }

    public function actions()
    {
        $actions = parent::actions();
        $actions['index']['prepareDataProvider'] = [$this, 'prepareDataProvider'];
        unset($actions['update']);

        return $actions;
    }

    public function prepareDataProvider()
    {
        $query = User::find()->with('subscription');
        return  new ActiveDataProvider([
            'query' => $query
        ]);
    }

    /**
     * @param $id
     * @return static
     * @throws HttpException
     */
    public function actionUpdate($id)
    {
        $bodyParams = \Yii::$app->request->bodyParams;

        $model = User::findOne($id);
        if($model === null){
            throw new HttpException(404, 'Model not found');
        }

        if($model->load(\Yii::$app->request->bodyParams, '') && $model->validate()){
            $model->save();

            if($bodyParams['subscription']){
                $subscription = ($model->subscription) ? $model->subscription : new Subscription();
                $subscription->attributes = $bodyParams['subscription'];
                $model->manageSubscription($subscription);
            }

        }
        return $model;
    }


}