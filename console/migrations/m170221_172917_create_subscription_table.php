<?php

use yii\db\Migration;

/**
 * Handles the creation of table `subscription`.
 */
class m170221_172917_create_subscription_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('subscription', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'expired' => $this->integer(11)
        ]);

        $this->addForeignKey('FKSubscriptionUser', 'subscription', 'user_id', 'user', 'id', 'CASCADE', 'CASCADE');
        $this->createIndex('IExpired', 'subscription', 'expired');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('subscription');
    }
}
