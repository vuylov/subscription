<?php
namespace console\controllers;
use yii\console\Controller;

class SubscriptionController extends Controller
{
    public function actionDeleteExpired()
    {
        $deletedRows = \Yii::$app->db->createCommand("DELETE FROM subscription WHERE expired < :now", [
            ':now' => strtotime('now')
        ])->execute();

        echo 'Deleted rows: '.$deletedRows;
    }
}