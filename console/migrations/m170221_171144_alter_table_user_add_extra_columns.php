<?php

use yii\db\Migration;

class m170221_171144_alter_table_user_add_extra_columns extends Migration
{
    public function safeUp()
    {
        $this->addColumn('user', 'last_name', $this->string()->after('email'));
        $this->addColumn('user', 'first_name', $this->string()->after('last_name'));
        $this->addColumn('user', 'middle_name', $this->string()->after('first_name'));
    }

    public function safeDown()
    {
        $this->dropColumn('user', 'last_name');
        $this->dropColumn('user', 'first_name');
        $this->dropColumn('user', 'middle_name');
    }
}
