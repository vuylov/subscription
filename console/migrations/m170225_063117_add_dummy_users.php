<?php

use yii\db\Migration;

class m170225_063117_add_dummy_users extends Migration
{
    public function up()
    {
        $nowTimeStamp = \time();
        $this->batchInsert('user',
            ['username', 'auth_key', 'password_hash', 'password_reset_token', 'email', 'last_name', 'first_name', 'middle_name', 'status', 'created_at', 'updated_at'],
            [
                [
                    'admin',
                    Yii::$app->security->generateRandomString(),
                    \Yii::$app->security->generatePasswordHash('admin'),
                    \Yii::$app->security->generateRandomString() . '_' . time(),
                    'admin@admin.ru',
                    'Бородач',
                    'Александр',
                    'Родионович',
                    10,
                    $nowTimeStamp,
                    $nowTimeStamp
                ],
                [
                    'user',
                    Yii::$app->security->generateRandomString(),
                    \Yii::$app->security->generatePasswordHash('user'),
                    \Yii::$app->security->generateRandomString() . '_' . time(),
                    'user@user.ru',
                    'Пользователев',
                    'Пользователь',
                    'Пользователевич',
                    10,
                    $nowTimeStamp,
                    $nowTimeStamp
                ]
            ]);
    }

    public function down()
    {
        $this->execute(
            'SET FOREIGN_KEY_CHECKS = 0;
            truncate table user;
            SET FOREIGN_KEY_CHECKS = 1;'
        );
    }
}
